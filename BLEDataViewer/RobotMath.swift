//
//  RobotMath.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 10/28/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import Foundation

func clip(value: Float, min: Float, max: Float) -> Float {
    if value > max{
        return max
    } else if value < min{
        return min
    }
    return value
}

func rad2Deg(_ rad:Float) -> Float {
    return rad * 180/Float.pi
}

func deg2Rad(_ deg: Float) -> Float {
    return deg * Float.pi / 180
}

func angDist(_ ang1: Float, _ ang2: Float) -> Float {
    // convert to double for higher precision
    let x = Double(ang1)
    let y = Double(ang2)
    return Float(atan2(sin(x-y), cos(x-y)))
}

func dist(x1:Float, x2:Float, y1:Float, y2: Float) -> Float{
    return Float(sqrt(pow(x2-x1,2) + pow(y2-y1, 2)))
}

func timeDist(t1: DispatchTime, t2: DispatchTime) -> Float {
    return Float(Double(t2.uptimeNanoseconds/1000000)/1e3 - Double(t1.uptimeNanoseconds/1000000)/1e3)
}
