//
//  ViewController.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 9/16/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import UIKit
import MetalKit
import AVKit

var ALIVE: Bool = true

func shouldContinueDataRefresh() -> Bool {
    return ALIVE
}

func colorized(_ string: String, _ color: UIColor) -> NSMutableAttributedString {
    var attributedString = NSMutableAttributedString()

    attributedString = NSMutableAttributedString(string: string)

    attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(location:0, length:string.count))

    return attributedString
}

func colorized_bool(_ bool: Bool) -> NSMutableAttributedString {
    let colormap = [true: UIColor.blue, false: UIColor.red]
    let str = String(bool)
    let color = colormap[bool]!
    return colorized(str, color)
}

let possibleLocations = [
    Location(x: 0, y: 0, angle: 0, name: "Starting Location", videoName: ""),
    Location(x: 18*12, y: -18, angle: 90, name: "Location 1", videoName: "location1"),
    Location(x: (18+28)*12, y: 18, angle: -90, name: "Location 2", videoName: "location2"),
    Location(x: (28+18+18)*12, y: -18, angle: 90, name: "Location 3", videoName: "location3"),
]

class ViewController: UIViewController {
    
    @IBOutlet var terminalDisplay : UITextView!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var stopButton: UIButton!
    @IBOutlet var resetPostionButton: UIButton!

    @IBOutlet var forwardButton: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var leftButton: UIButton!
    @IBOutlet var rightButton: UIButton!

    @IBOutlet weak var locationsDisplay: UILabel!
    @IBOutlet weak var locationButton1: UIButton!
    @IBOutlet weak var locationButton2: UIButton!
    @IBOutlet weak var locationButton3: UIButton!
    
    
    //    var animator: Animator?
    var overrideButtons: OverrideUI?
    var sensors: SensorsConnection?
    var robot: RobotController<DoubleRobot>?
    var onTour = false
    var videoPlayed = false
    var locations: [Location] = [possibleLocations[0]]

    // viewcontroller
    override func viewWillAppear(_ animated: Bool) {
        sensors = SensorsConnection()//SensorsComputer()
        self.view.backgroundColor = .darkGray
        super.viewWillAppear(animated)
        
        self.robot = RobotController(sensors:sensors)

        self.overrideButtons = OverrideUI(robot!, forwardButton: forwardButton, backButton: backButton, leftButton: leftButton, rightButton: rightButton)
        
        //animator = Animator(view: metalView)

        // configure ui
        terminalDisplay.text = "\n"
        self.terminalDisplay.backgroundColor = .black
        self.terminalDisplay.textColor = .lightGray
        self.terminalDisplay.isEditable = false
        
        self.startButton.titleLabel?.textColor = .white
        self.startButton.backgroundColor = .blue
        
        self.stopButton.titleLabel?.textColor = .white
        self.stopButton.backgroundColor = .red
        
        self.resetPostionButton.titleLabel?.textColor = .white
        self.resetPostionButton.backgroundColor = .orange

        // start any update threads
        self.updateSensorDisplay()
        self.updateLocationsGUI()
    }
    
    func updateLocationsGUI() {
        var guistring = ""
        for loc in self.locations {
            guistring += loc.name + "\n"
        }
        self.locationsDisplay.text = guistring
    }
    
    func updateLocations(locationSelected:Location){
        var locationFound = false
        for (i, loc) in self.locations.enumerated() {
            if loc.name == locationSelected.name {
                locationFound = true
                self.locations.remove(at: i)
                break
            }
        }
        if !locationFound {
            self.locations.append(locationSelected)
        }
        self.updateLocationsGUI()
    }
    
    @IBAction func locationButtonPressed1(_ sender: Any) {
        let location = possibleLocations[1]
        updateLocations(locationSelected: location)
    }

    @IBAction func locationButtonPressed2(_ sender: Any) {
        let location = possibleLocations[2]
        updateLocations(locationSelected: location)
    }

    @IBAction func locationButtonPressed3(_ sender: Any) {
        let location = possibleLocations[3]
        updateLocations(locationSelected: location)
    }
    
    func playVideo(videoFile: String) {
        guard !self.videoPlayed else {
            print("ignoring playVideo because video has already been played")
            return
        }
//        sleep(1)
        guard let path = Bundle.main.path(forResource: "videos/"+videoFile, ofType: "mov") else {
            print("ERROR \(videoFile) .mov not found")
            return
        }
        self.videoPlayed = true
        ALIVE = false

        let bounds = UIScreen.main.bounds
        let myNewView=UIView(frame: bounds)
        myNewView.backgroundColor=UIColor.black
//        myNewView.layer.cornerRadius=25

        self.view.addSubview(myNewView)
        self.view.bringSubviewToFront(myNewView)
        let video = AVPlayer(url: URL(fileURLWithPath: path))
        let playerLayer = AVPlayerLayer(player: video)
        playerLayer.frame = myNewView.bounds
        myNewView.layer.addSublayer(playerLayer)
        video.play()

        if let currentItem = video.currentItem {
            let duration = currentItem.asset.duration
            let deadlineTime = DispatchTime.now() + .seconds(Int(duration.seconds))
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                print("video closing")
                myNewView.removeFromSuperview()
                ALIVE = true
            }
        } else {
            print("error video not work")
        }
    }

    
    @IBAction func startButtonPressed (_ sender: Any) {
        self.videoPlayed = false
        if !self.onTour {
            self.onTour = true
            if let r = robot {
                let s = simplePlan(chosenLocations: locations)
                r.execute(steps: s)
            }
        } else {
            if let r = robot {
                r.resume()
            }
        }
//        playVideo(videoFile: "location1")
//        print("DONE DONE DONE")
    }

    @IBAction func stopButtonPressed(_ sender: Any) {
        if let r = robot {
            r.stop()
        }
    }
    
    @IBAction func resetButtonPressed(_ sender: Any) {
        if self.onTour {
            self.onTour = false
            if let r = robot {
                if r.mode != .stop {
                    r.stop()
                }
            }
        } else {
            let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to zero the position?", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                print("Position reset")
                self.videoPlayed = false
                if let r = self.robot {
                    r.robot.resetStateVars()
                }
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                print("Cancel button tapped")
            }
            
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            self.present(dialogMessage, animated: true, completion: nil)
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func updateSensorDisplay(){
        let queue = OperationQueue()
        queue.addOperation() {
            // do something in the background
            while true {
                usleep(500000)
                
                DispatchQueue.main.async {
                    guard shouldContinueDataRefresh() else { return }
                    // when done, update your UI and/or model on the main queue
//                    self.terminalDisplay.text = ""
                    if let r = self.robot {
                        
                        var track = false
                        if let track_connected = r.robot.sensors?.connected["pos"] {
                            track = track_connected
                        }
                        var lidar = false
                        if let lidar_connected = r.robot.sensors?.connected["scan"] {
                            lidar = lidar_connected
                        }
                        self.terminalDisplay.text = "Robot Connected:\t\(r.robot.connected)\nt265 Connected: \t\(track)\nLidar Connected:\t\(lidar)\nMode:\(r.mode), X:\(r.robot.stateVars.xPos), Y:\(r.robot.stateVars.yPos), Ang:\(rad2Deg(r.robot.stateVars.angle))\n"

                        if r.mode == .stop && self.videoPlayed == false {
                            let x = r.robot.stateVars.xPos
                            let y = r.robot.stateVars.yPos
                            let ang = r.robot.stateVars.angle
                            print("ang \(rad2Deg(ang)), pos \([x, y])")
                            for loc in self.locations {
                                if dist(x1:x, x2:loc.x, y1:y, y2:loc.y) < 36 && angDist(deg2Rad(loc.angle), ang) < deg2Rad(20.0) {
                                    self.playVideo(videoFile: loc.videoName)
                                    break
                                }
                            }
                            }
                    }
                }
            }
        }
    }

}


