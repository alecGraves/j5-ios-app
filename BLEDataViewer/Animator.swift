//
//  Animator.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 10/27/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

// thanks https://donaldpinckney.com/metal/2018/07/05/metal-intro-1.html (accessed 11/27)

import Foundation
import Metal
import MetalKit

//import ModelIO

class Animator: NSObject, MTKViewDelegate {
    let view: MTKView
    let device: MTLDevice?
    let commandQueue: MTLCommandQueue?
    let pipelineState: MTLRenderPipelineState?


    init(view argView: MTKView){
        view = argView
        device = MTLCreateSystemDefaultDevice()
        if let d = device {
            print("Animator device \(d) initialized!")
            commandQueue = d.makeCommandQueue()
            if let _ = commandQueue {
                print("Animator command queue initialized!")
            } else {
                print("Animator ERROR: could not make command queue for metal device")
            }
            
            view.device = d
            view.clearColor = MTLClearColorMake(0.0, 0.5, 1.0, 1.0)
            view.enableSetNeedsDisplay = true
            view.translatesAutoresizingMaskIntoConstraints = false
//            view.depthStencilPixelFormat = .depth32Float
//            view.colorPixelFormat = .bgra8Unorm
        } else {
            print("Animator ERROR: Could not find Metal Device")
            commandQueue = nil
        }

        // Create the Render Pipeline
        do {
            pipelineState = try Animator.buildRenderPipelineWith(metalDevice: device!, metalKitView: view)
        } catch {
            print("Unable to compile render pipeline state: \(error)")
            pipelineState = nil
        }
        
        super.init()
        view.delegate = self
    }
    
    func draw(in view: MTKView) {
        guard let dev = device else {
            print("Animation ERROR: metal gpu not found")
            return
        }
        guard let queue = commandQueue else {
            print("Animator ERROR queue not initialized")
            return
        }
        
        guard let commandBuffer = queue.makeCommandBuffer() else {
            print("Animator ERROR: could not create command buffer")
            return
        }

        guard let renderer = view.currentRenderPassDescriptor else {
            print("Animator ERROR: could not find view.currentRenderPassDescriptor")
            return
        }
        
        // change clear color from black to red
        renderer.colorAttachments[0].clearColor = MTLClearColorMake(0, 0, 0, 1)
        
        guard let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderer) else {
            print("Animator ERROR: commandBuffer.makeRenderCommandEncoder()")
            return
        }
        
        // ... now make renderEncoder encode drawing commands like drawing triangles from vertex data
        // Setup render commands to encode
        // We tell it what render pipeline to use
        guard let state = pipelineState else {
            return
        }
        // Create our vertex data
        let vertices = [Vertex(color: [1, 0, 0, 1], pos: [-1, -1]),
                    Vertex(color: [0, 1, 0, 1], pos: [0, 1]),
                    Vertex(color: [0, 0, 1, 1], pos: [1, -1]),
                    Vertex(color: [0, 0, 1, 1], pos: [1, 1])]

        let vertexBuffer: MTLBuffer
        vertexBuffer = dev.makeBuffer(bytes: vertices, length: vertices.count * MemoryLayout<Vertex>.stride, options: [])!

        renderEncoder.setRenderPipelineState(state)
        // What vertex buffer data to use
        renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        // And what to draw
        renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 3)

        
        renderEncoder.endEncoding()
        
        guard let drawable = view.currentDrawable else {
            print("Animator ERROR: view.currentDrawable not found")
            return
        }
        
        // Tell Metal to send the rendering result to the MTKView when rendering completes
        commandBuffer.present(drawable)
        
        // send encoded buffer data to GPU
        commandBuffer.commit()

    }
    
    // handles resizing the view e.g. if ios device orientation changes
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        // empty for now
    }
    
    
    
    // Create our custom rendering pipeline, which loads shaders using `device`, and outputs to the format of `metalKitView`
    class func buildRenderPipelineWith(metalDevice: MTLDevice, metalKitView: MTKView) throws -> MTLRenderPipelineState {
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        
        // Setup the shaders in the pipeline
        let library = metalDevice.makeDefaultLibrary()
        pipelineDescriptor.vertexFunction = library?.makeFunction(name: "vertexShader")
        pipelineDescriptor.fragmentFunction = library?.makeFunction(name: "fragmentShader")

        
        // Setup the output pixel format to match the pixel format of the metal kit view
        pipelineDescriptor.colorAttachments[0].pixelFormat = metalKitView.colorPixelFormat
        pipelineDescriptor.depthAttachmentPixelFormat = MTLPixelFormat.depth32Float

        // Compile the configured pipeline descriptor to a pipeline state object
        return try metalDevice.makeRenderPipelineState(descriptor: pipelineDescriptor)

    }

    
    
}

