//
//  OverrideUI.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 11/11/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import UIKit
import Foundation

class OverrideUI {
    var controller: RobotController<DoubleRobot>
    
    var forward = false
    var back = false
    var left = false
    var right = false
    var reset = false
    
    var overrideAction: RobotControlAction

    init(_ controllerArg: RobotController<DoubleRobot>, forwardButton: UIButton, backButton: UIButton, leftButton: UIButton, rightButton: UIButton) {
        controller = controllerArg
        overrideAction = RobotControlAction(translate: 0, rotate: 0)

        forwardButton.addTarget(self, action:#selector(fdown), for: .touchDown)
        forwardButton.addTarget(self, action:#selector(fup), for: .touchUpInside)
        forwardButton.addTarget(self, action:#selector(fup), for: .touchUpOutside)
        
        backButton.addTarget(self, action:#selector(bdown), for: .touchDown)
        backButton.addTarget(self, action:#selector(bup), for: .touchUpInside)
        backButton.addTarget(self, action:#selector(bup), for: .touchUpOutside)
        
        leftButton.addTarget(self, action:#selector(ldown), for: .touchDown)
        leftButton.addTarget(self, action:#selector(lup), for: .touchUpInside)
        leftButton.addTarget(self, action:#selector(lup), for: .touchUpOutside)
        
        rightButton.addTarget(self, action:#selector(rdown), for: .touchDown)
        rightButton.addTarget(self, action:#selector(rup), for: .touchUpInside)
        rightButton.addTarget(self, action:#selector(rup), for: .touchUpOutside)
        
        updateLoop()
    }
    
    @objc func fdown(){
        forward = true
    }
    
    @objc func fup(){
        forward = false
        reset = true
    }
    
    @objc func bdown() {
        back = true
    }
    
    @objc func bup() {
        back = false
        reset = true
    }
    
    @objc func ldown() {
        left = true
    }
    
    @objc func lup() {
        left = false
        reset = true
    }
    
    @objc func rdown() {
        right = true
    }
    
    @objc func rup() {
        right = false
        reset = true
    }

    func updateLoop() {
        let queue = OperationQueue()
        queue.addOperation() {
        while true {
            usleep(100000)
            DispatchQueue.main.async {
                guard shouldContinueDataRefresh() else { return }
                if self.forward || self.back || self.left || self.right || self.reset {
                    if self.forward && !self.back{
                        self.overrideAction.translate = 1
                    } else if self.back && !self.forward {
                        self.overrideAction.translate = -1
                    } else {
                        self.overrideAction.translate = 0
                    }
                    if self.left && !self.right {
                        self.overrideAction.rotate = 1
                    } else if self.right && !self.left {
                        self.overrideAction.rotate = -1
                    } else {
                        self.overrideAction.rotate=0
                    }
                    self.controller.override(action: self.overrideAction)
                    self.reset = false
            }
            }
        }
        }
    }
}
