//
//  SensorsConnection.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 11/1/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import Foundation

class SensorsConnection {
    let server: String
    var data: [String: [String: AnyObject]] = [:]
    var connected: [String: Bool]
    var updated = false

    init(serverAddress: String="http://192.168.1.2:8090") {
        if let _ = URL(string: serverAddress) {
            server = serverAddress
        } else {
            server = "http://192.168.1.2:8090"
        }

        data = [:]
        connected = ["pos": false,
                     "scan": false]
        subscribe(topic:"pos", freq:32);
        subscribe(topic:"scan", freq:4);
    }

    func getData() -> [String: [String: AnyObject]] {
        self.updated = false
        return self.data
    }

    func subscribe(topic: String, freq: Float64){
        let queue = OperationQueue()
        queue.addOperation() {
            // do something in the background
            var start = DispatchTime.now()
            var end = start
            let desiredTime = Int32((1/freq)*1e6) // us
            let sensorsURL = URL(string: self.server+"/"+topic)!

            while true {
                start = DispatchTime.now()

                // Set the URL the request is being made to.
                let request = URLRequest(url: sensorsURL, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 0.5)
                // Perform the request
                let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
                let dataAttempt = try? NSURLConnection.sendSynchronousRequest(request, returning: response)

                if let data = dataAttempt {
                    // Convert the data to JSON
                    let jsonSerialized = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : AnyObject]

                    if let json = jsonSerialized {
                        DispatchQueue.main.async {
                            guard shouldContinueDataRefresh() else { return }
                            self.data[topic] = nil
                            self.data[topic] = json
                            self.connected[topic] = true
                            self.updated = true
                        }
                    }
                     else {
                        DispatchQueue.main.async {
                            guard shouldContinueDataRefresh() else { return }
                            self.connected[topic] = false
                            print("error deserializing json")
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        guard shouldContinueDataRefresh() else { return }
                        self.connected[topic] = false
                    }

                }

                end = DispatchTime.now()
                let timeElapsed = Int32((end.uptimeNanoseconds - start.uptimeNanoseconds)/1000) // us
                usleep(UInt32(max(desiredTime-timeElapsed, 0)))
            }
        }
    }
    
    
}
