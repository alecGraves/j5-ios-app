//
//  BTTableViewManager.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 9/20/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import UIKit

class BTTableViewManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    var table: UITableView
    var btDevices: [Int]

    init(table: UITableView) {
        self.table = table
        self.btDevices = []
    }

    //tableviewdelegate/datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return btDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let btDeviceCell = tableView.dequeueReusableCell(withIdentifier: "btdevice", for:indexPath) as! BTTableDevice
        
        let idx: Int =  indexPath.row
        
        btDeviceCell.uuid?.text = String(btDevices[idx])
        btDeviceCell.rssi?.text = String(btDevices[idx]*3)
        btDeviceCell.backgroundColor = .lightGray
        btDeviceCell.uuid?.textColor = .white
        btDeviceCell.rssi?.textColor = .white
        
        return btDeviceCell
    }
    
}
