//
//  ShaderDefinitions.h
//  BLEDataViewer
//
//  Created by Alec Graves on 10/27/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

#ifndef ShaderDefinitions_h
#define ShaderDefinitions_h

#include <simd/simd.h>

struct Vertex {
    vector_float4 color;
    vector_float2 pos;
};

#endif /* ShaderDefinitions_h */
