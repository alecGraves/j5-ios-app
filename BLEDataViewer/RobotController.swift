//
//  RobotController.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 10/21/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import Foundation

enum ControlMode {
    case stop
    case override
    case drive
    case reverse
    case driveTo
    case turnTo
    case turn
    case deployKickstand
    case retractKickstand
    case done
}

struct Step {
    var mode: ControlMode
    var distance: Float = 0
    var distance2: Float = 0
}

enum KickstandState: Int32 {
    case unknown = 0
    case Deployed = 1
    case Retracted = 2
    case DeployingInProgress = 3
    case RetractingInProgress = 4
}

struct RobotStateVariables {
    var xPos: Float // ?
    var yPos: Float // ?
    var angle: Float // radians
    var batteryPercent: Float
    var poleHeightPercent: Float
    var kickStand: Int32
}

struct RobotControlAction {
    var translate: Float
    var rotate: Float
}

protocol Robot {
    var connected: Bool {get set}
    var stateVars: RobotStateVariables {get set}
    var stateVarsUpdated: Bool {get set}
    var action: RobotControlAction {get set}
    var sensors: SensorsConnection? {get set}
    var obstacleDetected: Bool {get set}
    
    init()
    init(sensors mySensors: SensorsConnection)
    func deployKickstand()
    func retractKickstand()
    func turn(degrees:Float)
    func resetStateVars()
}



class RobotController <T: Robot> {
    var mode: ControlMode
    var prevMode: ControlMode
    var modeStartTime: DispatchTime
    var distance: Float // degrees or inches
    var distance2: Float // inches, y

    var steps: [Step]
    var currentStepIdx: Int
    
    var robot: T!
    
    var turning = false
    var turningStartAngle: Float = 0
    var turningStopAngle: Float = 0
    
    var driving = false
    var driveStartAngle: Float = 0
    var driveStartX: Float = 0
    var driveStartY: Float = 0
    var driveStopX: Float = 0
    var driveStopY: Float = 0
    
    var overrideAction: RobotControlAction = RobotControlAction(translate:0, rotate: 0)

    init(sensors: SensorsConnection?=nil){
        mode = .stop
        prevMode = .stop
        modeStartTime = DispatchTime.now()
        distance = 0
        distance2 = 0
        
        steps = []
        currentStepIdx = 0
        
        if let s = sensors {
            robot = T(sensors:s)
        } else {
            robot = T()
        }
        
        controlLoop(hz:16)
    }
    
    func override(action: RobotControlAction) {
        if self.mode != .override {
            transition(to: .override)
        } else {
            modeStartTime = DispatchTime.now()
        }
        overrideAction = action
    }
    
    func execute(steps newSteps: [Step]) {
        // save the list of steps
        self.steps = newSteps
        self.currentStepIdx = 0
        
        // execute the first step
        let step = steps[0]
        transition(to: step.mode)
        distance = step.distance
        distance2 = step.distance2
    }
    
    func resume() {
        self.transition(to: prevMode)
    }
    
    func stop() {
        // transition to stop after kickstand is deployed
        if mode == .deployKickstand {
            self.driving = false
            self.turning = false
            self.prevMode = .stop
            return
        }

        transition(to: .stop)
//        self.driving = false
//        self.turning = false
        
        // deploy kick stand if already in stop mode
        if self.prevMode == .stop {
            transition(to: .deployKickstand)
        }
        
    }
    
    private func transition(to newMode: ControlMode) {
        self.prevMode = self.mode
        self.mode = newMode
        self.modeStartTime = DispatchTime.now()
        print("Switching robot to mode \(newMode)")
    }
    
    private func controlStep(){
        if self.robot.connected {
            switch self.mode {
            case .stop:
                robot.action = RobotControlAction(translate: 0, rotate: 0)
            case .deployKickstand:
                robot.action = RobotControlAction(translate: 0, rotate: 0)
                if robot.stateVars.kickStand == KickstandState.Deployed.rawValue {
                    if self.prevMode == .stop {
                        transition(to: .stop)
                    } else {
                        transition(to: .done)
                    }
                    break
                }
                if robot.stateVars.kickStand != KickstandState.DeployingInProgress.rawValue {
                    robot.deployKickstand()
                }
            case .retractKickstand:
                robot.action = RobotControlAction(translate: 0, rotate: 0)
                if robot.stateVars.kickStand == KickstandState.Retracted.rawValue {
                    if self.driving && self.prevMode == .drive {
                        transition(to: .drive)
                    } else if self.turning  && self.prevMode == .turn {
                        transition(to: .turn)
                    } else {
                        transition(to: .done)
                    }
                    break
                }
                if robot.stateVars.kickStand != KickstandState.RetractingInProgress.rawValue {
                    robot.retractKickstand()
                }
            case .override:
                let overrideTime = 10.0
                if (Double(DispatchTime.now().uptimeNanoseconds/1000000) - Double(modeStartTime.uptimeNanoseconds/1000000)) > overrideTime*1000 {
                    transition(to: prevMode)
                    break
                }
                robot.action = overrideAction
            case .turn:
                if self.turning == false {
                    self.turning = true
                    let currentAngle = robot.stateVars.angle
                    self.turningStartAngle = currentAngle
                    self.turningStopAngle = currentAngle + deg2Rad(self.distance)
    //                robot.turn(degrees: self.distance)
                }
                if robot.stateVars.kickStand == KickstandState.Deployed.rawValue {
                    transition(to: .retractKickstand)
                    break
                }
                let _currentAngle = robot.stateVars.angle
                let _dist = angDist(_currentAngle, turningStopAngle)
                let gain: Float = 1.0
                print("dist is \(deg2Rad(_dist))")
                robot.action = RobotControlAction(translate: 0, rotate: clip(value: -gain * _dist, min: -1, max: 1))

                print("turning, \(rad2Deg(angDist(_currentAngle, turningStopAngle)))), stopangle \(rad2Deg(turningStopAngle)), currentAngle \(rad2Deg(_currentAngle))")
                if abs(rad2Deg(angDist(_currentAngle, turningStopAngle))) < 0.5 {
                    self.turning = false
                    self.transition(to: .done)
                    break
                }
            case .reverse:
                let currentX = robot.stateVars.xPos
                let currentY = robot.stateVars.yPos
                let currentAng = robot.stateVars.angle
                print("x:\(currentX) , y:\(currentY), ang: \(currentAng)")
                if driving == false {
                    driving = true
                    driveStartX = currentX
                    driveStartY = currentY
                    driveStartAngle = currentAng
                    driveStopX = driveStartX + cos(currentAng) * distance
                    driveStopY = driveStartY + sin(currentAng) * distance
                }
                if robot.stateVars.kickStand == KickstandState.Deployed.rawValue {
                    transition(to: .retractKickstand)
                    break
                }
                
                //angError = angDist(currentAng, driveStartAngle) // TODO if ang > 100 degrees, direction should reverse? or transition to turn?
                let angAction = Float(0)
                print("stopping x:\(driveStopX), stopping y:\(driveStopY)")
                let posErrorIn = sqrt(pow(currentX-driveStopX, 2) + pow(currentY-driveStopY, 2))
                print("position error FT: \(posErrorIn / 12.0)")
                let translateAction = clip(value:-posErrorIn/36, min:-1, max:1)

                if posErrorIn < 6.0 {
                    self.driving = false
                    transition(to: .done)
                    break
                }
                
                robot.action = RobotControlAction(translate: translateAction, rotate: angAction)
                break
            case .drive:
                let currentX = robot.stateVars.xPos
                let currentY = robot.stateVars.yPos
                let currentAng = robot.stateVars.angle
                print("x:\(currentX) , y:\(currentY), ang: \(currentAng)")
                if driving == false {
                    driving = true
                    driveStartX = currentX
                    driveStartY = currentY
                    driveStartAngle = currentAng
                    driveStopX = driveStartX + cos(currentAng) * distance
                    driveStopY = driveStartY + sin(currentAng) * distance
                }
                if robot.stateVars.kickStand == KickstandState.Deployed.rawValue {
                    transition(to: .retractKickstand)
                    break
                }
                
                //angError = angDist(currentAng, driveStartAngle) // TODO if ang > 100 degrees, direction should reverse? or transition to turn?
                let angAction = Float(0)
                print("stopping x:\(driveStopX), stopping y:\(driveStopY)")
                let posErrorIn = sqrt(pow(currentX-driveStopX, 2) + pow(currentY-driveStopY, 2))
                print("position error FT: \(posErrorIn / 12.0)")
                let translateAction = clip(value:posErrorIn/24, min:-1, max:1)

                if posErrorIn < 6.0 {
                    self.driving = false
                    transition(to: .done)
                    break
                }
                
                if robot.obstacleDetected {
                    robot.action = RobotControlAction(translate: 0, rotate: 0)
                } else {
                    robot.action = RobotControlAction(translate: translateAction, rotate: angAction)
                }
                break
            case .driveTo:
                let targetX = self.distance
                let targetY = self.distance2
                let currentX = robot.stateVars.xPos
                let currentY = robot.stateVars.yPos
                let currentAng = robot.stateVars.angle
                let targetAngle = atan2(targetY-currentY, targetX-currentX)
                let driveDistance = sqrt(pow(targetX-currentX, 2) + pow(targetY-currentY, 2))
                let angError = angDist(currentAng, targetAngle)

                print("drive: \(driveDistance), turn: \(angError)")
                if abs(rad2Deg(angError)) > 5.0 && driveDistance > 6.0 {
                    let gain: Float = 1.0
                    let rotateAction = clip(value: -gain * angError, min: -1, max: 1)
                    robot.action = RobotControlAction(translate: 0, rotate: rotateAction)
                    break
                } else if driveDistance > 6.0 {
                    if robot.obstacleDetected {
                        robot.action = RobotControlAction(translate: 0, rotate: 0)
                    } else {
                        let gain: Float = 3.0
                        let rotateAction = clip(value: -gain * angError, min: -1, max: 1)
                        let translateAction = clip(value:driveDistance/36, min:-1, max:1)
                        robot.action = RobotControlAction(translate: translateAction, rotate: rotateAction)
                    }
                        break
                } else {
                    transition(to: .done)
                    break
                }
            case .turnTo:
                let currentAng = robot.stateVars.angle
                let targetAngle = deg2Rad(self.distance)
                let angError = angDist(currentAng, targetAngle)
                if abs(rad2Deg(angError)) > 0.5 {
                    let gain: Float = 1.0
                    let rotateAction = clip(value: -gain * angError, min: -1, max: 1)
                    robot.action = RobotControlAction(translate: 0, rotate: rotateAction)
                }  else {
                   transition(to: .done)
                   break
               }
            case .done:
                robot.action = RobotControlAction(translate: 0, rotate: 0)
                let doneWaitSeconds: Double = 0.5
                if (Double(DispatchTime.now().uptimeNanoseconds/1000000) - Double(modeStartTime.uptimeNanoseconds/1000000)) < doneWaitSeconds*1000 {
                    print("waiting")
                    return
                }
                // move to next step in list
                currentStepIdx += 1
                if currentStepIdx >= steps.count {
                    transition(to: .stop)
                    break
                }
                let step = steps[currentStepIdx]
                transition(to: step.mode)
                distance = step.distance
                distance2 = step.distance2
                break
            }
        }
    }
    
    private func controlLoop(hz loopRate:Float64 = 32){
        let queue = OperationQueue()
        queue.addOperation() {
            // do something in the background
            var start = DispatchTime.now()
            var end = start
            let desiredTime = Int32((1/loopRate)*1e6) // us
            
            while true {
                start = DispatchTime.now()

                DispatchQueue.main.async {
                    guard shouldContinueDataRefresh() else { return }
                    self.controlStep()
                }

                end = DispatchTime.now()
                let timeElapsed = Int32((end.uptimeNanoseconds - start.uptimeNanoseconds)/1000) // us
                //print("timeElapsed \(timeElapsed), timedesired \(desiredTime)")
                usleep(UInt32(max(desiredTime-timeElapsed, 0)))
            }
        }
    }
    
}


