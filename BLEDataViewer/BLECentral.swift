//
//  BLECentral.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 9/21/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import Foundation
import CoreBluetooth

struct Peripheral {
    var uuid: String
    var name: String
    var rssi: Double
    var peripheral: CBPeripheral
}

class SensorsComputer: BLECentral {
    // just a rename of below...
}

class BLECentral: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    var manager: CBCentralManager!
    var availablePeripherals: [String: Peripheral]
    var sensorsConnected: Bool
    var ref: CBPeripheral?
    var sensorServiceUUIDs: [String]
    var bleSensorComputerUUID: String
    var data: [String:[UInt8]]
    var dataNames: [String:[String:String]]

    override init() {
        data = [:]
        availablePeripherals = [:]
        sensorsConnected = false
        ref = nil
        sensorServiceUUIDs = ["5CA4"]
        //bleSensorComputerUUID = "096A1CFA-C6BC-A00C-5A68-3227F2C58C06" // builtin is shit
        bleSensorComputerUUID = "33548EF4-EDDE-6E6D-002F-DEEFC0A7AF99" // usb dongle

        dataNames = [
            // service uuids
            "5CA4": [
                // characteristic uuids
                "0000": "LidarRanges",
            ],
        ]

        super.init()
        manager = CBCentralManager(delegate: self, queue: nil)
        self.connect(uuid:bleSensorComputerUUID)
    }
    
    func scan(){
        let services = dataNames.map { CBUUID(string:$0.key) }
        manager.scanForPeripherals(withServices: services, options: nil)
    }
    
    func connect_internal_(uuid: String) -> Bool{ // TODO known bt device uuids.
        if self.sensorsConnected {
            // do not try to connect if already connected
            return true
        } else {
            print(self.availablePeripherals.count)
            if let found = self.availablePeripherals[uuid] {
                manager.stopScan()
                manager.connect(found.peripheral, options: nil)
                return true
            } else {
                if availablePeripherals.count == 0 {
                    scan()
                }
                print("Error! no peripheral with \(uuid) found!")
                return false
            }
        }
    }
    
    func connect(uuid: String){
        let queue = OperationQueue()
        queue.addOperation() {
            // do something in the background
            while true {
                //usleep(100000)
                sleep(1)
                OperationQueue.main.addOperation {
                    guard shouldContinueDataRefresh() else { return }
                    self.connect_internal_(uuid: self.bleSensorComputerUUID)
                }
            }
        }
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            print("The central is powered on!")
            scan() // automatically start scanning for BLE devices
        default:
            print("The centraol is NOT powered on (state is \(central.state))")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        var name = ""
        if let name_ = peripheral.name {
            name = name_
        }

        let uuid = peripheral.identifier.uuidString
        let rssi = Double(truncating: RSSI)
        availablePeripherals[peripheral.identifier.uuidString] = Peripheral(uuid: uuid, name: name, rssi: rssi, peripheral: peripheral)
        print(uuid, rssi)
    }
    
    func getSorted(uuids:Bool = false) -> [Peripheral] {
        let peripherals = Array(availablePeripherals.values)
        return peripherals.sorted(by:) {$0.rssi >= $1.rssi}
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Central connected!")
        sensorsConnected = true
        peripheral.delegate = self
        var cbuuids:[CBUUID] = []
        for id in sensorServiceUUIDs {
            cbuuids.append(CBUUID(string: id))
        }
        peripheral.discoverServices(cbuuids) // TODO store service uuids somewhere nicer
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        if let e = error {
            print("Central disconnected because \(e)")
        } else {
            print("Central disconnected! (no error)")
        }
        sensorsConnected = false
        availablePeripherals = [:]
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Central failed to connect...")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let error = error {
            print("Peripheral could not discover services! Because: \(error.localizedDescription)")
        } else {
            peripheral.services?.forEach({(service) in
                print("Service discovered \(service)")
                // TODO characteristics UUIDs known
                peripheral.discoverCharacteristics(nil, for: service)
            })
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let error = error {
            print("Could not discover characteristic because error: \(error.localizedDescription)")
        } else {
            service.characteristics?.forEach({ (characteristic) in
                print("Characteristic: \(characteristic)")
                if characteristic.properties.contains(.notify){
                    peripheral.setNotifyValue(true, for: characteristic)
                }
                if characteristic.properties.contains(.read){
                    peripheral.readValue(for: characteristic)
                }
                if characteristic.properties.contains(.write){
                    peripheral.writeValue(Data([1]), for: characteristic, type: .withResponse)
                }
            })
            
        }
    }

    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            print("error after didUpdateValueFor characteristic \(characteristic): \(error.localizedDescription)")
        } else {
            let sname = characteristic.service.uuid.uuidString
            let cname = characteristic.uuid.uuidString
            guard let _dname = dataNames[sname], let dataName = _dname[cname] else {
                return
            }
            //print("value of characteristic \(cname) in service \(sname) was updated.")
            if let value = characteristic.value {
                let value_arr = Array(value)
                //print("    The new data value is \(value_arr.count) bytes long")
                //print("dataname is \(dataName)")
                self.data[dataName] = value_arr
            }
        }
    }
}
