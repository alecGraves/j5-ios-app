//
//  Shaders.metal
//  BLEDataViewer
//
//  Created by Alec Graves on 10/27/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

#include <metal_stdlib>
#include "ShaderDefinitions.h"


using namespace metal;


struct VertexOut {
    float4 color;
    float4 pos [[position]];
};


vertex VertexOut vertexShader(const device Vertex *vertexArray [[buffer(0)]], unsigned int vid [[vertex_id]])
{
    VertexOut vOut;
    vOut.pos = float4(vertexArray[vid].pos, 1);
    vOut.color = vertexArray[vid].color;
    return vOut;
}

fragment float4 fragmentShader(VertexOut interpolated [[stage_in]])
{
    return interpolated.color;
}

