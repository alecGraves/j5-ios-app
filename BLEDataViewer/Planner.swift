//
//  Planner.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 11/18/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import Foundation

struct Location {
    var x: Float
    var y: Float
    var angle: Float
    var name: String
    var videoName: String
}


func simplePlan(chosenLocations: [Location]) -> [Step] {
    var steps: [Step] = []
    steps.append(Step(mode:.retractKickstand))
    for loc in chosenLocations {
        steps.append(Step(mode:.driveTo, distance:loc.x, distance2: loc.y))
        steps.append(Step(mode:.turnTo, distance: loc.angle))
//        steps.append(Step(mode:.reverse, distance:-abs(loc.y)))
        if loc.videoName.count > 0 { // stop and play video
            steps.append(Step(mode: .deployKickstand))
            steps.append(Step(mode: .stop))
            steps.append(Step(mode:.retractKickstand))
            steps.append(Step(mode:.driveTo, distance:loc.x, distance2: 0))
        }
    }
    return steps
}
