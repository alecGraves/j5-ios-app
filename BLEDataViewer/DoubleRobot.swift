//
//  DoubleRobot.swift
//  BLEDataViewer
//
//  Created by Alec Graves on 11/8/19.
//  Copyright © 2019 Alec Graves. All rights reserved.
//

import Foundation

class DoubleRobot: NSObject, Robot, DRDoubleDelegate {
    var connected: Bool
    var stateVars: RobotStateVariables
    var offset: RobotStateVariables
    var stateVarsUpdated: Bool
    var action: RobotControlAction
    var sensors: SensorsConnection?
    var obstacleDetected: Bool = false
    var obstacleCounter = 0

//    var sensorData: RobotStateVariables

    override required init() {
        self.connected = false
        self.stateVars = RobotStateVariables(xPos:0, yPos: 0, angle: 0, batteryPercent: 0, poleHeightPercent: 0, kickStand: 0)
        self.offset = RobotStateVariables(xPos:0, yPos: 0, angle: 0, batteryPercent: 0, poleHeightPercent: 0, kickStand: 0)
        self.stateVarsUpdated = false
        self.action = RobotControlAction(translate: 0, rotate: 0)
        
        super.init()
        DRDouble.shared().delegate = self
    }
    
    required init(sensors mySensors: SensorsConnection) {
        sensors = mySensors
        self.connected = false
        self.stateVars = RobotStateVariables(xPos:0, yPos: 0, angle: 0, batteryPercent: 0, poleHeightPercent: 0, kickStand: 0)
        self.offset = RobotStateVariables(xPos:0, yPos: 0, angle: 0, batteryPercent: 0, poleHeightPercent: 0, kickStand: 0)
        self.stateVarsUpdated = false
        self.action = RobotControlAction(translate: 0, rotate: 0)
        
        super.init()
        DRDouble.shared().delegate = self
        self.procSensors()
    }
    
    func doubleDidConnect(_ theDouble: DRDouble!) {
        print("Double Connected!!")
        theDouble.startTravelData()
        self.connected = true
    }
    
    func doubleDidDisconnect(_ theDouble: DRDouble!) {
        print("Double Disconnected!!")
        self.connected = false
    }
    
    func doubleStatusDidUpdate(_ theDouble: DRDouble!) {
        print("Double status update recieved")
        if let double = DRDouble.shared(){
//            self.stateVars.leftEncoder = double.leftEncoderDeltaInches
//            self.stateVars.rightEncoder = double.rightEncoderDeltaInches
            self.stateVars.batteryPercent = double.batteryPercent
            self.stateVars.poleHeightPercent = double.poleHeightPercent
            self.stateVars.kickStand = double.kickstandState
            stateVarsUpdated = true
        }
    }
    
    func doubleTravelDataDidUpdate(_ theDouble: DRDouble!) {
        if let s = self.sensors, s.connected["pos"] == true {
            //no update if using external sensors, todo incorporate
        } else {
            if let double = DRDouble.shared(){
                self.stateVars.angle += double.headingDeltaRadians
                self.stateVars.xPos += double.xDeltaInches * cos(self.stateVars.angle)
                self.stateVars.yPos += double.xDeltaInches * sin(self.stateVars.angle)
                self.stateVars.xPos += double.yDeltaInches * sin(self.stateVars.angle)
                self.stateVars.yPos += double.yDeltaInches * cos(self.stateVars.angle)
                stateVarsUpdated = true
            }
        }
    }
    
    func doubleDriveShouldUpdate(_ theDouble: DRDouble!) {
        theDouble.variableDrive(self.action.translate, turn: self.action.rotate)
    }
    
    func deployKickstand() {
        if stateVars.kickStand == KickstandState.DeployingInProgress.rawValue || stateVars.kickStand == KickstandState.Deployed.rawValue {
            return
        }
        if let double = DRDouble.shared() {
            double.deployKickstands()
        }
    }
    
    func retractKickstand() {
        if stateVars.kickStand == KickstandState.RetractingInProgress.rawValue || stateVars.kickStand == KickstandState.Retracted.rawValue {
            return
        }
        if let double = DRDouble.shared() {
            double.retractKickstands()
        }
    }
    
    func turn(degrees:Float){
        if let double = DRDouble.shared() {
            double.turn(byDegrees: -degrees)
        }
    }
    
    func resetStateVars(){
        // undo the rotation
        let a: Double = -Double(offset.angle) // angle in realsense frame
        let x: Double = Double(stateVars.xPos)
        let y: Double = Double(stateVars.yPos)
        let xPosRealsenseFrame = x * cos(a) + y * -sin(a)
        let yPosRealsenseFrame = x * sin(a) + y * cos(a)
        
        // subtract the offset
        let realReportedX = Float(xPosRealsenseFrame) - offset.xPos
        let realReportedY = Float(yPosRealsenseFrame) - offset.yPos

        let realReportedAngle = stateVars.angle - offset.angle
        self.offset = RobotStateVariables(xPos:-1*realReportedX, yPos: -1*realReportedY, angle: -1*realReportedAngle, batteryPercent: 0, poleHeightPercent: 0, kickStand: 0)
        print("RESET, state vars offset: \(self.offset)")
        self.stateVars = RobotStateVariables(xPos:0, yPos: 0, angle: 0, batteryPercent: 0, poleHeightPercent: 0, kickStand: 0)
        if let double = DRDouble.shared() {
            self.stateVars.batteryPercent = double.batteryPercent
            self.stateVars.poleHeightPercent = double.poleHeightPercent
            self.stateVars.kickStand = double.kickstandState
        }
    }
    
    
    func procSensors(){
        let loopRate:Double = 32.0
        let queue = OperationQueue()
        queue.addOperation() {
            // do something in the background
            var start = DispatchTime.now()
            var end = start
            let desiredTime = Int32((1/loopRate)*1e6) // us
            
            while true {
                start = DispatchTime.now()
                DispatchQueue.main.async {
                    guard shouldContinueDataRefresh() else { return }
                    if let sensors = self.sensors{
                        if sensors.connected["pos"] == true && sensors.updated {
                            let sensorData = sensors.getData()
                            if let data = sensorData["pos"], let pose=data["pose"] {
                                //                                print("\(pose)")
                                if let orientation = pose["orientation"] as? [String:Double],
                                    var w = orientation["w"], var x = orientation["x"], var y = orientation["y"], var z = orientation["z"]{
                                    x = 0
                                    y = 0
                                    let mag = sqrt(w*w + z*z)
                                    w /= mag
                                    z /= mag
                                    //                                    let heading = 2*acos(w)
                                    let heading = -atan2(z, w)*2
                                    self.stateVars.angle = Float(heading) + self.offset.angle
//                                    print(heading)
                                    self.stateVarsUpdated = true
                                }
                                if let position = pose["position"] as? [String:Double], let x = position["x"], let y = position["y"] {
//                                    print("postion w:, \(position)")
                                    
                                    // apply offset
                                    let x_offset = Double(Float(x*39.37008) + self.offset.xPos) // meter to inches
                                    let y_offset = Double(-Float(y*39.37008) + self.offset.yPos)
//                                    print("x \(Float(x*39.37008)), x offset \(self.offset.xPos)")
//                                    print("y \(Float(y*39.37008)), y offset \(self.offset.yPos)")

            
                                    // rotate to robot coordinate frame
                                    let a = -Double(self.offset.angle) // invert angle for back to z-up coordinate frame
                                    self.stateVars.xPos = Float(x_offset * cos(a) + y_offset * sin(a))
                                    self.stateVars.yPos = Float(x_offset * -sin(a) + y_offset * cos(a))
                                }
                            }
                            if let scanData = sensorData["scan"], let ranges = scanData["ranges"] as? [Double?], let angleMin = scanData["angle_min"] as? Double, let angleMax = scanData["angle_max"] as? Double {
                                
//                                print("min \(angleMin), max \(angleMax), len \(ranges.count)")
                                var scanPoints: [[Double]] = []

                                let angleIncriment = (angleMax - angleMin) / Double(ranges.count)
                                var angle = angleMin
                                for rMaybe in ranges {
                                    if let range = rMaybe {
                                        let rangeIn = range*39.37008 // meter to inches
                                        let rotate = -Double.pi //-Double.pi / 2
                                        let x = rangeIn*cos(angle + rotate)
                                        let y = rangeIn*sin(angle +  rotate)
                                        scanPoints.append([x, y])
                                    }
                                    angle += angleIncriment
                                }
                                
                                var obstacle = false
                                for pt in scanPoints {
                                    let x = pt[0]
                                    let y = pt[1]
//                                    print("x: \(x), y: \(y)")
                                    if x > 6 && x < 48 && y > -12 && y < 12 {
                                        obstacle = true
                                    }
                                }
                                
                                let max_obstacle_count = 10
                                if obstacle && self.obstacleCounter < max_obstacle_count {
                                    self.obstacleCounter += 1
                                }
                                if !obstacle && self.obstacleCounter > 0 {
                                    self.obstacleCounter -= 1
                                }
                                
                                if self.obstacleCounter > max_obstacle_count / 2 {
                                    self.obstacleDetected = true
                                } else {
                                    self.obstacleDetected = false
                                }
//                                print("Obstacle Detected: \(self.obstacleDetected)")
                            }
                        } // dispatchqueue async
                    } // sensors connected
                }

                end = DispatchTime.now()
                let timeElapsed = Int32((end.uptimeNanoseconds - start.uptimeNanoseconds)/1000) // us
                //print("timeElapsed \(timeElapsed), timedesired \(desiredTime)")
                usleep(UInt32(max(desiredTime-timeElapsed, 0)))
            }
        }
    }
}
